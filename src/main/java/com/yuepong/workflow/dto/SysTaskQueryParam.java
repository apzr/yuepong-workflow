package com.yuepong.workflow.dto;

import lombok.Data;

@Data
public class SysTaskQueryParam {

    public String id;
    public String sKey;
    public String sId;
    public String taskId;
    public String route;

}

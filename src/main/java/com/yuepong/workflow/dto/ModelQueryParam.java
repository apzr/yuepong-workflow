package com.yuepong.workflow.dto;

import lombok.Data;

/**
 * ModelQueryParam
 * <p>
 * <br/>
 *
 * @author apr
 * @date 2021/10/25 16:38:27
 **/
@Data
public class ModelQueryParam {

    private String id;
    private String instanceId;

}
